@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　利用者マスタ</h3>
<br>
<div class="row">
    <div class="col-md-8">
        @if(Session::has('msg'))
        <p  class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('msg') }}</p>
        @endif
    </div>
    <div class="col-md-4">
        <form action="{{route('user.create')}}" class="form-group" method="GET">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4 "><button class="btn btn-md btn-secondary w-100">新規登録</button></div>
            </div>
        </form>
    </div>
</div>


<div class="card text-center" >
    <div>
        <table class=" table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">契約顧客ID</th>
                    <th scope="col">契約顧客名</th>
                    <th scope="col">メールアドレス</th>
                    <th scope="col">Baseplacement</th>
                    <th scope="col"></th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach ($listUsers as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    @if($user->baseplacement!= null)
                    <td > {{$user->baseplacement->baseplacement_name}}</td>
                    @else
                    <td>No baseplacement</td>
                    @endif
                    <td>
                        <a href="{{route('user.edit', $user->id)}}"><input type="button" class="btn btn-success" value="編集"></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-6">{{ $listUsers->links('vendor.pagination.bootstrap-4') }}</div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>

@endsection
