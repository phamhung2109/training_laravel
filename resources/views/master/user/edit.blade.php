@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　契約顧客登録・編集・削除</h3>
<br>
<div class="col-md-8">
    @if(Session::has('msg'))
    <p  class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('msg') }}</p>
    @endif
</div>
<div class="card">
    <form action="{{route('user.update', $user->id)}}" method="post" id="my_form" >
        @method('PATCH')
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">メールアドレス</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle ">必須</span></div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" name="email" class="form-control @error('email') is-invalid  @enderror " id="staticEmail" value="{{old('email')? old('email'): $user->email}} ">
                            @error('email')
                            <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">利用者名</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle ">必須</span></div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" name="name" class="form-control @error('name') is-invalid  @enderror " id="staticEmail" value="{{old('name')? old('name'): $user->name}}">
                            @error('name')
                            <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">拠点</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle ">必須</span></div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <select class="custom-select form-control @error('baseplacement_id') is-invalid  @enderror" name="baseplacement_id" id="staticEmail" >
                                @if($user->baseplacement != null)
                                    <option value="{{$user->baseplacement_id}}">{{$user->baseplacement->baseplacement_name}}</option>
                                    @foreach ($listBaseplacements as $baseplacement)
                                        <option value="{{$baseplacement->id}}">{{$baseplacement->baseplacement_name}}</option>
                                    @endforeach
                                @else
                                    <option value=""></option>
                                    @foreach ($listBaseplacements as $baseplacement)
                                        <option value="{{$baseplacement->id}}">{{$baseplacement->baseplacement_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('baseplacement_id')
                            <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-9">
            <div class="text-center " >
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{route('user.index')}}"><input type="button" class="btn btn-md btn-success w-50"  value="戻る"></a>
                    </div>
                    <div class="col-md-4">
                        <form action="{{route('user.destroy', $user->id)}}" method="post">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-md btn-danger w-50" >変更</button>
                        </form>
                    </div>
                    
                    <div class="col-md-4">
                        <button  name="submit" onclick="update()" id="show-loading" class="btn btn-md btn-success w-50" >削除</button>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')
    {{-- get element with id= show-loading --}}
    <script type="text/javascript" src="{{asset('js/master/base/process.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/master/base/delete.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/confirm_delete.js')}}"></script>
    @endsection