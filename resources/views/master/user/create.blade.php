@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　契約顧客登録・編集・削除</h3>
<div class="row">
    <div class="col-md-7 w-50" >
        @if(Session::has('msg'))
        <p  class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('msg') }}</p>
        @endif
    </div>
</div>
<div class="card">
    <form action="{{route('user.store')}}" method="POST" >
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">メールアドレス</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" >
                            @error('email')
                            <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">パスワード</label>
                        {{-- <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div> --}}
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" value="" name="password" >
                            @error('password')
                            <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">利用者名</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" name="name" >
                            @error('name')
                            <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">拠点</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <select class="custom-select" id="" name="baseplacement_id" >
                                @foreach ($listBaseplacements as $base)
                                <option selected value="{{$base->id}}">{{$base->baseplacement_name}}</option>
                                @endforeach
                            </select>
                            @error('baseplacement_id')
                            <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-9">
                <div class="text-center" >
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{route('user.index')}}"><input type="button" class="btn btn-md btn-success w-25 "  value="戻る"></a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-md btn-success w-25" id="show-loading" >登録</button>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
{{-- get element with id= show-loading --}}
<script type="text/javascript" src="{{asset('js/master/base/process.js')}}"></script>
@endsection