@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　拠点登録・編集・削除</h3>
<br>
<div class="col-lg-8">
    @if(Session::has('msg'))
    <p  class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('msg') }}</p>
    @endif
</div>
<div class="card">
    <form action="{{route('baseplacement.store')}}" method="POST"  >
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">拠点名</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control @error('baseplacement_name') is-invalid @enderror" value="{{ old('baseplacement_name') }}" name="baseplacement_name" >
                            @error('baseplacement_name')
                            <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-9">
                <div class="text-center" >
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <a href="{{route('baseplacement.index')}}"><input type="button" class="btn btn-md btn-success w-25"  value="戻る"></a>
                        </div>
                        <div class="col-lg-6">
                            <button class="btn btn-md btn-success w-25" id="show-loading">登録</button>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('scripts')
{{-- get element with id= show-loading --}}
<script type="text/javascript" src="{{asset('js/master/base/process.js')}}"></script>
@endsection