@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　拠点マスタ</h3>
<br>
<div class="row">
    <div class="col-lg-8">
        @if(Session::has('msg'))
        <p  class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('msg') }}</p>
        @endif
    </div>
    <div class="col-lg-4">
        <form action="" class="form-group">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4"></div>
                <div class="col-lg-4"><a href="{{route('baseplacement.create')}}"><input type="button" class="btn btn-md btn-secondary w-100"  value="新規登録"></a></div>
            </div>
        </form>
    </div>
</div>

<div class="card text-center">
    <div >
        <table class=" table table-bordered">
            <div>
                <div class="col-lg-8">
                    <thead class="thead-dark" >
                        <tr style="text-align: center; ">
                            <th scope="col" >拠点名</th>
                            <th scope="col"></th>
                            
                        </tr>
                    </thead>
                </div>
                <div class="col-lg-4">
                    <tbody >
                        @foreach ($listBaseplacements as $listBase)
                        
                        <tr style="text-align: center; ">
                            <th scope="row">{{$listBase->baseplacement_name}}</th>
                            <td><a href="{{route('baseplacement.edit', $listBase->id)}}"><input type="button" class="btn btn-success" value="編集"></a></td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </div>
                
            </div>
        </table>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-6">{{ $listBaseplacements->links('vendor.pagination.bootstrap-4') }}</div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>



@endsection