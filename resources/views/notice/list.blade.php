@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　お知らせ一覧</h3>
<br>
<div class="row">
    <div class="col-lg-8">
        @if(Session::has('msg'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('msg') }}</p>
        @endif
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <a href="{{route('notice.create')}}"><button class="btn btn-md btn-secondary w-100">新規登録</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class=" table table-bordered text-center">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">タイトル</th>
                    <th scope="col">公開開始日時</th>
                    <th scope="col">公開終了日時</th>
                    <th scope="col">対象</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($listNotice as $item)
                <tr>
                    <th scope="row">{{$item->TITLE}}</th>
                    <td>{{$item->REGISTRATIONDATE}}</td>
                    <td>{{$item->EXPIRATIONDATETIME}}</td>
                    <td>全て</td>
                    <td><a href="{{route('notice.edit', $item->ANNOUNCEMENTID)}}"><input type="button" class="btn btn-success" value="編集"></a></td>
                </tr>
                @endforeach
        </table>
    </div>
    <div class="col-sm-12">
        <nav class="nav justify-content-center">
            {!! $listNotice->appends($_GET)->links() !!}
        </nav>
    </div>
</div>

@endsection