@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}" type="text/css">
@endsection
@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　お知らせ登録</h3>
<br>
<div class="card">
    <form action="{{route('notice.update', $notice->ANNOUNCEMENTID)}}" method="POST" id="noticeFormEdit">
        @method('PATCH')
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">タイトル</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle ">必須</span></div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" name="title" class="form-control" value="{{$notice->TITLE}}">
                            @error('title')
                                <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">公開開始日時</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" name="registrationDate" class="form-control registrationDate" value="{{$notice->REGISTRATIONDATE}}">
                            @error('registrationDate')
                                <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">公開終了日時</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" name="expirationDateTime" class="form-control expirationDateTime" value="{{$notice->EXPIRATIONDATETIME}}">
                            @error('expirationDateTime')
                                <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">通知対象契約顧客</label>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="row">
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="tenantID" id="tenantID" value="{{$notice->TENANTID}}" readonly>
                                </div>
                                <div class="col-sm-2">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-md btn-primary w-100" data-toggle="modal" data-target="#exampleModal">
                                        戻る
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade mt-5" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-bordered text-center" id="tableTenant">
                                                        <thead class="thead-dark">
                                                            <tr>
                                                                <th scope="col">TENANTID</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="clickableRow">
                                                                <td>1</th>
                                                            </tr>
                                                            <tr class="clickableRow">
                                                                <td>2</th>
                                                            </tr>
                                                            <tr class="clickableRow">
                                                                <td>3</th>
                                                            </tr>
                                                            <tr class="clickableRow">
                                                                <td>4</th>
                                                            </tr>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" id="saveTenantId">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-sm-1">
                            <button type="button" class="btn btn-success rounded-circle" aria-label="Close"><i class="fa fa-times"></i></button>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">本文</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11">
                            <textarea class="form-control" rows="4" name="body">{{$notice->BODY}}</textarea>
                            @error('body')
                                <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="form-group row">
        <div class="col-sm-3"></div>
        <div class="col-sm-9">
            <div class="row text-center">
                <div class="col-sm-3">
                    <a href="{{route('notice.index')}}"><input type="button" class="btn btn-md btn-success w-50" value="戻る"></a>
                </div>
                <div class="col-sm-3">
                    <form action="{{route('notice.destroy', $notice->ANNOUNCEMENTID)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-md btn-danger w-50">削除</button>
                    </form>
                </div>
                <div class="col-sm-3">
                    <button type="submit" class="btn btn-md btn-success w-50" id="noticeEditButton">変更</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/notice/edit_delete.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/confirm_delete.js')}}"></script>
@endsection