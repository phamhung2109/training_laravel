@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　支払通知</h3>

<br>
<div class="card">
    <form action="">
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <div class="row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">支払通知月</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11"><input type="text" class="form-control" id="staticEmail"></div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <div class="row text-center">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-3">
                            <button class="btn btn-md btn-success w-50">検索</button>
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<br>
<div class="row">
    <div class="col-lg-8"></div>
    <div class="col-lg-4">
        <form action="" class="form-group">
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <button class="btn btn-md btn-secondary w-100">支払通知書作成</button>
                </div>
                <div class="col-sm-4">
                    <button class="btn btn-md btn-secondary w-100">ダウンロード</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class=" table table-bordered text-center">
            <thead class="thead-dark">
                <tr>
                    <th scope="col"><input type="checkbox" id="gridCheck1">全選択</th>
                    <th scope="col">契約顧客名</th>
                    <th scope="col">作成状況</th>
                    <th scope="col">ダウンロード状況</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row" class="text-center"><input type="checkbox" id="gridCheck1"></th>
                    <td>取手市文化事業団</td>
                    <td>未作成</td>
                    <td>未ダウンロード</td>
                </tr>
                <tr>
                    <th scope="row" class="text-center"><input type="checkbox" id="gridCheck1"></th>
                    <td>取手市文化事業団</td>
                    <td>未作成</td>
                    <td>未ダウンロード</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection