@extends('layouts.master')
@section('content')
<h3 class="mt-4">Edit phone</h3>
<br>
<div class="card">
    <form action="{{route('baseplacement.update', $baseplacement->id)}}" method="post" id="my_form"  >
        @method('PATCH')
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">拠点名</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11">
                            <input name="baseplacement_name" type="text" class="form-control @error('baseplacement_name') is-invalid  @enderror " required value="{{$baseplacement->baseplacement_name}}">
                            @error('baseplacement_name')
                                <h6 class="text-danger">(*){{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-9">
            <div class="text-center" >
                <div class="row">
                    <div class="col-lg-4">
                        <a href="{{route('baseplacement.index')}}"><input type="button" class="btn btn-md btn-success w-50"  value="戻る"></a>
                    </div>
                    <div class="col-lg-4">
                        <form action="{{route('baseplacement.destroy', $baseplacement->id)}}" method="post">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-md btn-danger w-50" >変更</button>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        <button  name="submit" onclick="update()" id="show-loading" class="btn btn-md btn-success w-50" >削除</button>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
{{-- get element with id= show-loading --}}
<script type="text/javascript" src="{{asset('js/master/base/process.js')}}"></script>
<script type="text/javascript" src="{{asset('js/master/base/delete.js')}}"></script>
<script type="text/javascript" src="{{asset('js/confirm_delete.js')}}"></script>
@endsection