<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav" style="margin-left:20px">
                <a href="{{route('contract')}}" class="nav-link collapsed" >
                    契約顧客
                </a>
                <a class="nav-link collapsed" href="{{route('search')}}" >
                    イベント
                </a>
                <a class="nav-link collapsed" href="{{route('notice.index')}}" >
                    お知らせ
                </a>
                <a class="nav-link collapsed" href="{{route('support-notice')}}" >
                    支払通知
                </a>
                <a class="nav-link collapsed" href="{{route('billing')}}" >
                    精算・請求
                </a>
                <a class="nav-link collapsed" href="{{route('phone.index')}}">
                    Phone
                </a>
                <div class="dropdown show">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                        マスタ
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link" href="{{route('baseplacement.index')}}">Base</a>
                            <a class="nav-link" href="{{route('user.index')}}">User</a>
                        </nav>
                    </div>
                </div>
                
            </div>
        </div>
    </nav>
</div>