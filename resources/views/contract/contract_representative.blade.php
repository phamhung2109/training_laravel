@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　契約顧客登録・編集・削除</h3>
<br>
<div class="row">
    <div style="margin-left: 25px">契約顧客担当者</div>
</div>
<br>

<div >
    <form action="" >
        <div class="card text-center w-100" >
            <div class="card-body" id="form1" >
                <div class="login-form" >
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">契約顧客担当者名</label>
                                <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                            </div>
                        </div>
                        
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-11">
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" >
                                    @error('email')
                                    <h6 style="color: red">(*){{ $message }}</h6>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">契約顧客担当者部署名</label>
                                <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                            </div>
                        </div>
                        
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-11">
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" >
                                    @error('email')
                                    <h6 style="color: red">(*){{ $message }}</h6>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">役割分担</label>
                                <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                            </div>
                        </div>
                        
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-11">
                                    <select class="custom-select" id="staticEmail" >
                                        <option selected>副担当</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
        
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-8"></div>
                <div class="col-lg-1"></div>
                <div class="col-lg-2">
                    <input type="button" class="btn btn-primary btn-md w-50"  onclick="add()" style="margin-right: 75px" value="さらに追加">
                </div>
            </div>
            <br>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-5"></div>
            <div class="col-lg-2">
                <a href="{{route('contract-infor')}}"><input type="button" class="btn btn-md btn-success w-50" value="次へ"></a>
            </div>
            <div class="col-lg-5"></div>
        </div>
    </form>
</div>

@endsection
<script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>