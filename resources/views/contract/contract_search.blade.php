@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　契約顧客一覧</h3>
<br>
<div class="row">
    <div class="col-lg-8">＞検索条件</div>
    <div class="col-lg-4">
        <form action="" class="form-group">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4"></div>
                <div class="col-lg-4"><button class="btn btn-md btn-primary w-100" >ダウンロード  </button></div>
            </div>
        </form>
    </div>
    
    <div class="card text-center w-100" >
        <table class=" table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th></th>
                    <th scope="col">イベントID</th>
                    <th scope="col">契約顧客名</th>
                    <th scope="col">イベント名</th>
                    <th scope="col">施設名</th>
                    <th scope="col">開催日時</th>
                    <th scope="col">発売日時</th>
                    <th scope="col">販売方法</th>
                    
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>販売中</th>
                    <th scope="row">1</th>
                    <td>取手市文化事業団</td>
                    <td>XXX</td>
                    <td>市民会館</td>
                    <td>2020/04/15 10:00</td>
                    <td>2020/03/10 10:00</td>
                    <td>窓口＋Web</td>
                </tr>
                <tr>
                    <th>終了</th>
                    <th scope="row">2</th>
                    <td>REDEE</td>
                    <td>YYY</td>
                    <td>REDEE</td>
                    <td>2020/05/25 18:00</td>
                    <td>2020/03/11 10:0</td>
                    <td>窓口</td>
                </tr>
                <tr>
                    <th>未開催</th>
                    <th scope="row">2</th>
                    <td>IC</td>
                    <td>ZZZ</td>
                    <td>ICホール</td>
                    <td>2020/06/02 19:00</td>
                    <td>2020/03/12 10:00</td>
                    <td>Web</td>
                </tr>
                
            </tbody>
        </table>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-5"></div>
                <div class="col-lg-2">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active" aria-current="page">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-5"></div>
            </div>
        </div>
    </div>
</div>

@endsection