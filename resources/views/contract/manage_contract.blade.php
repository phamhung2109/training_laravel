@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　契約顧客登録・編集・削除</h3>
<br>
<div class="row">
    <div style="margin-left: 25px">契約顧客情報</div>
</div>
<br>

<div class="card">
    <form >
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">契約顧客名</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">契約顧客名（表示用）</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">ログイン許可</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="radio" name="status" id="staticEmail" value="1" checked="checked"> 許可する &nbsp;
                            <input type="radio" name="status" id="staticEmail" value="0"> 許可しない
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">郵便番号</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">都道府県</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">市区町村・町域</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">番地・建物</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">電話番号</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">FAX番号</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="row">
                        <label for="" class="col-sm-6 col-form-label">メールアドレス</label>
                        <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="email" >
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-lg-5"></div>
            <div class="col-lg-2 text-center">
                <a href="{{route('representative')}}"><input type="button" class="btn btn-md btn-success w-50" value="次へ"></a>
            </div>
            <div class="col-lg-5"></div>
        </div>
        <br>
    </form>
</div>
<br>



@endsection