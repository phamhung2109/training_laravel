@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　契約顧客登録・編集・削除</h3>
<br>
<div class="row">
    <div style="margin-left: 25px">契約情報</div>
</div>
<br>

<div >
    <form action="" >
        <div class="card w-100" >
            <div class="card-body" id="form1" >
                <div class="login-form" >
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">契約サービス</label>
                                <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-11">
                                    <input type="radio" name="status" id="staticEmail" value="1" checked="checked"> チケット for Web &nbsp;
                                    <input type="radio" name="status" id="staticEmail" value="0"> チケット for LINE Direct
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">窓口販売手数料</label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="custom-select" id="staticEmail" >
                                        <option selected>率</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="staticEmail" value="email@example.com">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">プレイガイド委託販売手数料</label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="custom-select" id="staticEmail" >
                                        <option selected>率</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="staticEmail" value="email@example.com">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">インターネット販売手数料</label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="custom-select" id="staticEmail" >
                                        <option selected>率</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="staticEmail" value="email@example.com">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">決済手数料</label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="custom-select" id="staticEmail" >
                                        <option selected>率</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="staticEmail" value="email@example.com">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-3">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">決済手数料</label>
                                <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                            </div>
                        </div>
                        
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" >
                                    @error('email')
                                    <h6 style="color: red">(*){{ $message }}</h6>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-5"></div>
            <div class="col-lg-2">
                <a href="{{route('contract-connection')}}"><input type="button" class="btn btn-md btn-success w-50" value="次へ"></a>
            </div>
            <div class="col-lg-5"></div>
        </div>
    </form>
</div>

@endsection
