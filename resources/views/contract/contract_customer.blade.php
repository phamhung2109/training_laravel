@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　契約顧客一覧</h3>
<br>
<div class="row">
    <div class="col-lg-8"></div>
    <div class="col-lg-4">
        <form action="" class="form-group">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><input type="text" class="form-control" placeholder="契約顧客名"></div>
                <div class="col-lg-3"><button class="btn btn-md btn-primary w-100" >検索  </button></div>
                <div class="col-lg-3 "><a href="{{route('manage-contract')}}"><input type="button" class="btn btn-md btn-secondary w-100"  value="新規登録"></a></div>
            </div>
        </form>
    </div>
    
    <div class="card text-center" style="width: 100%;">
        <table class=" table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">契約顧客ID</th>
                    <th scope="col">契約顧客名</th>
                    <th scope="col">電話番号</th>
                    <th scope="col">メールアドレス</th>
                    <th scope="col">代表者</th>
                    <th scope="col">契約サービス</th>
                    <th scope="col"></th>
                    
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>契約顧客1</td>
                    <td>00-0000-0000</td>
                    <td>xxx@xxx.com</td>
                    <td>代表者1</td>
                    <td>Web Direct</td>
                    <td><button class="btn btn-success">編集</button></td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>契約顧客2</td>
                    <td>00-0000-0001</td>
                    <td>yyy@yyy.com</td>
                    <td>代表者2</td>
                    <td>Web</td>
                    <td><button class="btn btn-success">編集</button></td>
                </tr>
            </tbody>
        </table>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-5"></div>
                <div class="col-lg-2">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active" aria-current="page">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-5"></div>
            </div>
        </div>
    </div>
</div>

@endsection