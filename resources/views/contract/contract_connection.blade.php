@extends('layouts.master')
@section('content')
<h3 class="mt-4">｜　契約顧客登録・編集・削除</h3>
<br>
<div class="row">
    <div style="margin-left: 25px">接続情報</div>
</div>
<br>

<div >
    <form action="" >
        <div class="card w-100 " >
            <div class="card-body" id="form1" >
                <div class="login-form" >
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">決済手数料</label>
                                <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div>
                            </div>
                        </div>
                        
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-11">
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" >
                                    @error('email')
                                    <h6 style="color: red">(*){{ $message }}</h6>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">構築場所</label>
                                {{-- <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div> --}}
                            </div>
                        </div>
                        
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-11">
                                    <select class="custom-select" id="staticEmail" >
                                        <option selected>副担当</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
        
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">DBホスト名</label>
                                {{-- <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div> --}}
                            </div>
                        </div>
                        
                        <div class="col-sm-10">
                           <p>DBホスト名</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-2">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">DBポート番号</label>
                                {{-- <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div> --}}
                            </div>
                        </div>
                        
                        <div class="col-sm-10">
                           <p>DBポート番号</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-2">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">DB名称</label>
                                {{-- <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div> --}}
                            </div>
                        </div>
                        
                        <div class="col-sm-10">
                           <p>DB名称</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-2">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">DBアカウント</label>
                                {{-- <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div> --}}
                            </div>
                        </div>
                        
                        <div class="col-sm-10">
                           <p>DBアカウント</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-2">
                            <div class="row">
                                <label for="" class="col-sm-6 col-form-label">DBパスワード</label>
                                {{-- <div class="col-sm-6 text-center"><span class="badge badge-danger align-middle">必須</span></div> --}}
                            </div>
                        </div>
                        
                        <div class="col-sm-10">
                           <p>
                            DBパスワード</p>
                        </div>
                    </div>

                   
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-2">
                <button class="btn btn-success w-50">完了</button>
            </div>
            <div class="col-md-5"></div>
        </div>
    </form>
</div>

@endsection
