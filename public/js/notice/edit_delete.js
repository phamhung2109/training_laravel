$(document).ready(function () {
    // Click submit edit form
    $("#noticeEditButton").click(function (e) {
        show_loader();
        $("#noticeFormEdit").submit();
    });

    // Click value table tenant add background color
    $('#tableTenant').on('click', 'tbody tr', function (event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
    });

    // Chooose value tenant and show value into input 通知対象契約顧客
    $('#saveTenantId').on('click', function (event) {
        var valueTenantId = $(".highlight").find("td").text().trim();
        $('#exampleModal').modal('hide');
        $('#tenantID').val(valueTenantId);
    });

    // Datetimepicker input registrationDate
    $('.registrationDate').datetimepicker({
        timepicker:false,
        format:'Y-m-d'
    });

    // Datetimepicker input expirationDateTime
    $('.expirationDateTime').datetimepicker({
        format: 'Y-m-d H:i'
    });
});
