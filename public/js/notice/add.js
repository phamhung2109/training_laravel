$(document).ready(function () {
    // Click submit form
    $("#notice-add-button").click(function (e) {
        show_loader();
    });

    // Click value table tenant add background color
    $('#tableAddTenant').on('click', 'tbody tr', function (event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
    });

    // Chooose value tenant and show value into input 通知対象契約顧客
    $('#saveAddTenant').on('click', function (event) {
        var valueTenantId = $(".highlight").find("td").text().trim();
        $('#modalNoticeAdd').modal('hide');
        $('#tenantID').val(valueTenantId);
    });

    $('.registrationDate').datetimepicker({
        timepicker:false,
        format:'Y-m-d'
    });

    $('.expirationDateTime').datetimepicker({
        format: 'Y-m-d H:i'
    });
});
