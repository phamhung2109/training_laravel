var hide_loader = () => {
    $("#overlay").fadeOut(300);
};

var show_loader = () => {
    $("#overlay").fadeIn(300);
}