// confirm when delete data
var SweetAlert2Demo = function() {
    //== Demos
    var initDemos = function() {
        $('.btn-danger').click(function(e) {
            var $form =  $(this).closest("form");
            e.preventDefault();
            swal({
                title: 'Are you sure to delete?',
                text: "",
                type: 'warning',
                buttons:{
                    confirm: {
                        text : 'Delete',
                        className : 'btn btn-success'
                    },
                    cancel: {
                        visible: true,
                        text: 'Cancel',
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                
                if (Delete) {
                    if (Delete) {
                        show_loader();
                        $form.submit();
                    }
                    swal({
                        title: user_id,
                        text: 'Your file has been deleted.',
                        type: 'success',
                        buttons : {
                            confirm: {
                                className : 'btn btn-success'
                            }
                        }
                    });
                } else {
                    swal.close();
                }
            });
        });
    };
    return {
        //== Init
        init: function() {
            initDemos();
        },
    };
}();

//== Class Initialization
$(document).ready(function() {
    SweetAlert2Demo.init();
});