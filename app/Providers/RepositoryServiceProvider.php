<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // regist for repository
        $repositories = [
            'User\UserRepositoryInterface' => 'User\UserRepository',
            'Baseplacemst\BaseplacemstRepositoryInterface' => 'Baseplacemst\BaseplacemstRepository',
            'Announcement\AnnouncementRepositoryInterface' => 'Announcement\AnnouncementRepository'
        ];

        foreach ($repositories as $interface => $class) {
            $this->app->bind("App\\Repositories\\$interface", "App\\Repositories\\$class");
        }

        // // regist for serice
        // $serices = [
        //     'Batch\Dhcp\BatchDhcpServiceInterface' => 'Batch\Dhcp\BatchDhcpService',
        //     'Batch\V4fixed\BatchV4fixedServiceInterface' => 'Batch\V4fixed\BatchV4fixedService',
        //     'Batch\V6Plus\BatchV6PlusServiceInterface' => 'Batch\V6Plus\BatchV6PlusService',
        //     'Batch\Maintaince\BatchMaintenanceServiceInterface' => 'Batch\Maintaince\BatchMaintenanceService',
        // ];

        // foreach ($serices as $interface => $class) {
        //     $this->app->bind("App\\Services\\$interface", "App\\Services\\$class");
        // }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
