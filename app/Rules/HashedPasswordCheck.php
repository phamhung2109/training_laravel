<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash; // <-- notice.

class HashedPasswordCheck implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // here you get the hashed name stored in your database (?)
        $hashedPassword = \App\Models\User::find(1)->password;

        // next, you compare this with the received value.
        return Hash::check($value, $hashedPassword);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute does not match with the stored value.';
    }
}