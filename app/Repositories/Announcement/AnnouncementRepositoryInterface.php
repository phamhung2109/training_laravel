<?php
namespace App\Repositories\Announcement;

interface AnnouncementRepositoryInterface
{
    public function getModel();
}