<?php
namespace App\Repositories\Announcement;

use App\Repositories\BaseRepository;
use App\Models\Announcement;

class AnnouncementRepository extends BaseRepository implements AnnouncementRepositoryInterface
{
    // get Model Announcement
    public function getModel()
    {
        return Announcement::class;
    }

        
}