<?php
namespace App\Repositories\Baseplacemst;

use Illuminate\Http\Request;

interface BaseplacemstRepositoryInterface
{
    public function getModel();

    public function getList();
}