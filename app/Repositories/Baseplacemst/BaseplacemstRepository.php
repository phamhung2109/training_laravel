<?php

namespace App\Repositories\Baseplacemst;

use App\Models\Baseplacemst;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BaseplacemstRepository extends BaseRepository implements BaseplacemstRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return Baseplacemst::class;
    }

    public function getList()
    {
        return Baseplacemst::all();
    }
}
