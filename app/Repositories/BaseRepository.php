<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

abstract class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function __construct()
    {
        $this->setModel();
    }

    //get Model
    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }

    public function getAll()
    {
        // return $this->model->all();
        return $this->model->paginate(MAX_RECORD_LIST);
    }

    public function find($id)
    {
        $result = $this->model->find($id);

        return $result;
    }

    public function create($attributes = [])
    {
        DB::beginTransaction();
        try {
            $result = $this->model->create($attributes);
            DB::commit();
            return $result;
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e);
        }
    }

    public function update($id, $attributes = [])
    {
        DB::beginTransaction();
        try {
            $result = $this->find($id);
            if ($result) {
                $result->update($attributes);
                DB::commit();
                return $result;
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e);
        }
    }

    public function delete($id)
    {
        try {
            $result = $this->find($id);
            if ($result) {
                $result->delete();
                return $result;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
