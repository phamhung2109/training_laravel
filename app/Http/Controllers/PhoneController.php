<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    public function index(){
        $phone = Phone::all();
        return view('phone.index', compact('phone'));
    }

    public function edit(){

    }

    public function create(){
        return view('phone.create');
    }

    public function show(){

    }

    public function update(){}

    public function store(Request $request){
        $phone = new Phone;

        $phone->name = $request->name;
        $phone->model = $request->model;
        $phone->save();
        if($phone){
            return redirect()->route('phone.index');
        }else{
            dd('error');
        }
    }
}
