<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Announcement\AnnouncementRepositoryInterface;

class NoticeController extends Controller
{
    public function __construct(
        AnnouncementRepositoryInterface $announcement
    )
    {
        $this->announcementRepository = $announcement;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $listNotice = $this->announcementRepository->getAll();
        $listNotice = $this->announcementRepository->getAll();
        return view('notice.list', compact('listNotice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notice.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = [
            'title' => 'required',
            'registrationDate' => 'required',
            'expirationDateTime' => 'required',
            'body' => 'required',
        ];
        $request->validate($validate);

        $attributes = [
            'title' => $request->title,
            'registrationDate' => $request->registrationDate,
            'expirationDateTime' => $request->expirationDateTime,
            'tenantID' => $request->tenantID,
            'body' => $request->body,
            'updateDate' => date("Y-m-d H:i:s"),
            'version' => 0,
        ];

        $this->announcementRepository->create($attributes);
        return redirect()->route('notice.index')->with('msg', CREATE_SUCCESS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = $this->announcementRepository->find($id);
        return view('notice.edit_delete', compact('notice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = [
            'title' => 'required',
            'registrationDate' => 'required',
            'expirationDateTime' => 'required',
            'body' => 'required',
        ];
        $request->validate($validate);

        $attributes = [
            'title' => $request->title,
            'registrationDate' => $request->registrationDate,
            'expirationDateTime' => $request->expirationDateTime,
            'tenantID' => $request->tenantID,
            'body' => $request->body,
            'updateDate' => date("Y-m-d H:i:s"),
            'version' => 0,
        ];
        $this->announcementRepository->update($id, $attributes);
        return redirect()->route('notice.index')->with('msg', UPDATE_SUCCESS);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->announcementRepository->delete($id);
        return redirect()->route('notice.index')->with('msg', DELETE_SUCCESS);
    }
}
