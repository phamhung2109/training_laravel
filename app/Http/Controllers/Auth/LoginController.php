<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use App\Rules\HashedPasswordCheck;
use Auth;
use View;
use Validator;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $userRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepositoryInterface $userRepo)
    {
        // $this->middleware('is.login')->except('logout');
        $this->userRepo = $userRepo;
    }

    /**
     * Show login form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login(Request $request)
    {
        return View::make('login.index');
    }

    /**
     * User authentication
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doLogin(Request $request)
    {
        $input = $request->all();
        // validate the info, create rules for the inputs

        $rules = array(
            'email'    => 'required|email|exists:users,email', // make sure the email is an actual email
            'password' => ['required', new HashedPasswordCheck], // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make($input, $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput(); // send back all errors to the login form
            // return $request->input();
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email'     => $input['email'],
                'password'  => $input['password']
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                return Redirect::to('dashboard');
            } else {
                return back()->withErrors($validator)->withInput(); // send back all errors to the login form
                // Redirect::back()->withInput($request->all());
            }
        }
    }

    public function doLogout()
    {   
        Auth::logout(); // log the user out of our application
        return Redirect::to('login'); // redirect the user to the login screen
    }
}
