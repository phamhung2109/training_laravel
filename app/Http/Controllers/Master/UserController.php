<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Baseplacemst;
use App\Models\Phone;
use App\Models\Role;
use App\Models\User;
use App\Repositories\Baseplacemst\BaseplacemstRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller

{
    public function __construct(UserRepositoryInterface $_user, BaseplacemstRepositoryInterface $_baseplacement)
    {
        $this->_user = $_user;
        $this->_baseplacement = $_baseplacement;
    }

    public function index()
    {
        $listUsers = $this->_user->getAll();
        return view('master.user.index', compact('listUsers'));
    }

    public function create()
    {
        $listBaseplacements = $this->_baseplacement->getList();
        return view('master.user.create', compact('listBaseplacements'));
    }

    public function  store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'baseplacement_id' => 'required'
        ]);

        $attributes = [
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'baseplacement_id' => $request->baseplacement_id,
        ];

        $user = $this->_user->create($attributes);

        if (isset($user)) {
            return redirect()->route('user.index')->with('msg', CREATE_SUCCESS);
        } else {
            return back()->with('msg', CREATE_ERROR);
        }
    }

    public function edit($id)
    {
        $listBaseplacements = Baseplacemst::all();

        //one to one
        $base = Baseplacemst::find(1);
        // dd($base->user->name);

        //one to one reverse
        $user = User::find($id);
        // dd($user->baseplacement->baseplacement_name);

        //one to many
        $user= User::find($id)->phone;
        foreach($user as $u){
            // dd($u->name);
        }

        //one to many reverse
        $phone = Phone::find(2);
        // dd($phone->user->name);

        //many to many
        $user = User::find(1);
        // dd($user->roles());
        foreach ($user->role as $role) {
            // dd($role->name);
        }
        //many to many reverse
        $user1 = User::find(1);
        foreach($user1->role as $role){
            dd($role->pivot->user_id);
        }




        
        // dd($user->baseplacement->baseplacement_name);
        return view('master.user.edit', compact('user', 'listBaseplacements'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'baseplacement_id' => 'required'
        ]);

        $attributes = [
            'email' => $request->email,
            'name' => $request->name,
            'baseplacement_id' => $request->baseplacement_id,
        ];

        $user = $this->_user->update($id, $attributes);

        if ($user) {
            return redirect()->route('user.index')->with('msg', UPDATE_SUCCESS);
        } else {
            return back()->with('msg', UPDATE_ERROR);
        }
    }

    public function destroy($id)
    {
        $user = $this->_user->delete($id);
        return redirect()->route('user.index')->with('msg', DELETE_SUCCESS);
    }
}
