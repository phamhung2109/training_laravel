<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Baseplacemst;
use App\Repositories\Baseplacemst\BaseplacemstRepositoryInterface;
use Illuminate\Http\Request;

class BaseplacemstController extends Controller
{
    public function __construct(BaseplacemstRepositoryInterface $_baseplacement)
    {
        $this->_baseplacement = $_baseplacement;
    }

    public function index()
    {
        $listBaseplacements = $this->_baseplacement->getAll();
        // dd($listBaseplacements);
        return view('master.baseplacement.index', compact('listBaseplacements'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'baseplacement_name' => 'required',
        ]);

        $attributes = [
            'baseplacement_name' => $request->baseplacement_name,
        ];

        $baseplacement = $this->_baseplacement->create($attributes);

        if (isset($baseplacement)) {
            return redirect()->route('baseplacement.index')->with('msg', CREATE_SUCCESS);
        } else {
            return back()->with('msg', CREATE_ERROR);
        }
    }

    public function create()
    {
        return view('master.baseplacement.create');
    }

    public function edit($id)
    {
        $baseplacement = Baseplacemst::find($id);
        dd($baseplacement->user);
        return view('master.baseplacement.edit', compact('baseplacement'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'baseplacement_name' => 'required',
        ]);

        $attributes = [
            'baseplacement_name' => $request->baseplacement_name,
        ];

        $baseplacement = $this->_baseplacement->update($id, $attributes);

        if (isset($baseplacement)) {
            return redirect()->route('baseplacement.index')->with('msg', UPDATE_SUCCESS);
        } else {
            return back()->with('msg', UPDATE_ERROR);
        }
    }

    public function destroy($id)
    {
        $baseplacement = $this->_baseplacement->delete($id);
        return redirect()->route('baseplacement.index')->with('msg', DELETE_SUCCESS);
    }
}
