<?php

define('NOT_DELETED', 0);

define('IS_DELETED', 1);

// Max record in 1 page
define('MAX_RECORD_LIST', 20);

// Max record in 1 file csv
define('MAX_RECORD_FILE_CSV', 10);

define('CREATE_SUCCESS', 'Create success');

define('UPDATE_SUCCESS', 'Update success');

define('DELETE_SUCCESS', 'Delete success');

define('CREATE_ERROR', 'Error in create');

define('UPDATE_ERROR', 'Error in update');