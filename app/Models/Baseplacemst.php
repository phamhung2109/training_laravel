<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Baseplacemst extends Model
{
    //
    protected $table = 'baseplacemst';

    protected $fillable = ['baseplacement_name'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id');
    }
}
