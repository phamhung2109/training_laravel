<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = 'announcement';
    protected $primaryKey = 'ANNOUNCEMENTID';
    protected $fillable =['title', 'registrationDate', 'expirationDateTime', 'tenantID', 'body', 'updateDate', 'version'];
    public $timestamps = false;
}
