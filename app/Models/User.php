<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password', 'baseplacement_id'];

    protected $hidden = ['password', 'remember_token'];

    public function baseplacement()
    {
        return $this->hasOne('App\Models\Baseplacemst', 'id', 'baseplacement_id');
    }

    public function phone()
    {
        return $this->hasMany('App\Models\Phone', 'user_id', 'id');
    }

    public function role(){
        return $this->belongsToMany('App\Models\Role');
    }
}
