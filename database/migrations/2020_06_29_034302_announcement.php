<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Announcement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement', function (Blueprint $table) {
            $table->bigIncrements('ANNOUNCEMENTID', 11);
            $table->date('REGISTRATIONDATE');
            $table->string('TITLE', 50);
            $table->string('BODY', 1000);
            $table->dateTime('EXPIRATIONDATETIME', 0);
            $table->integer('TENANTID')->nullable();
            $table->dateTime('UPDATEDATE', 0);
            $table->integer('VERSION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement');
    }
}
