<?php

use App\Models\Phone;
use App\Models\User;
use Illuminate\Database\Seeder;

class PhoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Phone::class, 1)->create();
    }
}
