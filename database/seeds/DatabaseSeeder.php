<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BaseTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(PhoneTableSeeder::class);
        $this->call(RoleTableSeeder::class);
    }
}
