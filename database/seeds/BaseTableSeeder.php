<?php

use App\Models\Baseplacemst;
use App\Models\User;
use Illuminate\Database\Seeder;

class BaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Baseplacemst::class, 1)->create();
    }
}
