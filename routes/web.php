<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::view('/login', 'login/index');

// Route::group(['middleware' => 'guest'], function () {

// });
Route::get('/login', 'Auth\LoginController@login')->name('login')->middleware('guest');;
Route::post('/login', 'Auth\LoginController@doLogin');
// Route::get('logout', array('uses' => 'Auth\LoginController@doLogout'));
Route::get('/logout', 'Auth\LoginController@doLogout')->name('logout');


    Route::view('/', 'dashboard/index')->name('dashboard');
    Route::view('/dashboard', 'dashboard/index')->name('dashboard');

    Route::view('/password-recovery', 'password_recovery/index')->name('password_recovery');

    Route::get('contract', function () {
        return view('contract/contract_customer');
    })->name('contract');

    Route::get('representative', function () {
        return view('contract/contract_representative');
    })->name('representative');

    Route::get('/manage-contract', function () {
        return view('contract/manage_contract');
    })->name('manage-contract');

    Route::get('contract-infor', function () {
        return view('contract/contract_infor');
    })->name('contract-infor');

    Route::get('contract-connection', function () {
        return view('contract/contract_connection');
    })->name('contract-connection');

    Route::get('contract-search', function () {
        return view('contract/contract_search');
    })->name('search');

    Route::resource('/baseplacement', 'Master\BaseplacemstController');

    Route::resource('/user', 'Master\UserController');
    
    Route::resource('/notice', 'NoticeController');

    Route::view('support-notice', 'support_notice/index')->name('support-notice');

    Route::view('billing', 'billing/index')->name('billing');


Route::resource('/phone', 'PhoneController');
